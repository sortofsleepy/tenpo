#ifndef FxComposer_hpp
#define FxComposer_hpp
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "cinder/Log.h"
#include <memory>
#include <vector>
#include "post/Fxpass.hpp"

using namespace ci;
using namespace std;

typedef std::shared_ptr<class FxComposer> FxComposerRef;

class FxComposer {
	vector<FxpassRef> passes;
public:
	FxComposer() {}

	static FxComposerRef create() {
		return FxComposerRef(new FxComposer());
	}

	void addPass(FxpassRef pass) {
		passes.push_back(pass);
	}

	void compile(ci::gl::TextureRef mInput) {
		for (int i = 0; i < passes.size(); ++i) {
			if (i == 0) {
				passes[i]->setInputTexture(mInput);
			}
			else {
				passes[i]->setInputTexture(passes[i - 1]->getOutput());
			}
		}
	}

	void update(std::function<void(int index, FxpassRef pass)> func = nullptr) {
		for (int i = 0; i < passes.size(); ++i) {
			if (func != nullptr) {
				func(i, passes[i]);
			}
			else {
				passes[i]->update();
			}
		}
	}

	ci::gl::TextureRef getOutput() {
		return passes[passes.size() - 1]->getOutput();
	}
};

#endif 
