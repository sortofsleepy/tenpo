#pragma once


#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/gl.h"
#include "GodRays.h"
using namespace ci;
using namespace std;

class Background {

	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;

	ci::gl::FboRef mTexture;
	float time = 0.0;

	GodRays rays;
	ci::gl::TextureRef mInput;
public:
	Background() {}
	void setup(ci::gl::TextureRef mInput){
		this->mInput = mInput;
		setupTexture();
	
		rays.setInputTexture(mInput);
		mTexture = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight());
		gl::ScopedFramebuffer fbo(mTexture);
		gl::clear(ColorA(0, 0, 0, 0));
	}

	void setupTexture() {
		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset("passthru.glsl"));
		fmt.fragment(app::loadAsset("bg.frag"));

		vector<vec2> positions;
		positions.push_back(vec2(-1, -1));
		positions.push_back(vec2(-1, 4));
		positions.push_back(vec2(4, -1));

		gl::VboMesh::Layout layout;
		layout.attrib(geom::POSITION, 2);

		mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout });
		mMesh->bufferAttrib(geom::POSITION, sizeof(vec2) * 3, positions.data());
		mShader = gl::GlslProg::create(fmt);
		mBatch = gl::Batch::create(mMesh, mShader);

	}

	void updateTexture() {
		gl::ScopedFramebuffer fbo(mTexture);
		gl::clear(ColorA(0, 0, 0, 0));
		gl::ScopedModelMatrix mat;
		time += 0.01;
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());

		mBatch->getGlslProg()->uniform("resolution", vec2(app::getWindowWidth(), app::getWindowHeight()));
		mBatch->getGlslProg()->uniform("time", time);
		mBatch->draw();
	}

	void draw() {
		rays.update();
		rays.draw(mInput);
		// render final scene
		//gl::setMatricesWindow(app::getWindowSize());
		//gl::viewport(app::getWindowSize());
		//gl::draw(rays.getOutput(), app::getWindowBounds());
	}


};