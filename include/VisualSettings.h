#pragma once

#include "cinder/Vector.h"
#include "cinder/Log.h"
#include <memory>
using namespace ci;

typedef std::shared_ptr<class VisualSettings> SettingsRef;
class VisualSettings
{
private:
	VisualSettings() {}


public:
	float uLength = 100.0f;
	float uNumSeg = 100.0f;
	float uEnd = 40.0;

	static VisualSettings * getInstance() {
		static VisualSettings * instance = new VisualSettings();
		return instance;
	}

	void setLength(float uLength) {
		this->uLength = uLength;
	}

	void setEnd(float uEnd) {
		this->uEnd = uEnd;
	}

	void setNumSeg(float uNumSeg) {
		this->uNumSeg = uNumSeg;
	}


};