#pragma once

#include "WindowTypes.h"
#include "cinder/gl/Fbo.h"
#include "ArcCam.h"
#include "cinder/app/App.h"	

#include "CubedCube.h"
#include "ParticleSystem.h"
#include "LineSystem.hpp"

#include "post/Fxpass.hpp"
#include "post/FxComposer.hpp"
#include "AudioPlayer.h"
#include "VisualSettings.h"
#include "Background.h"

class MainApp : public BasicWindow {

	// cube object
	CubedCube cubes;

	// main object camera
	ArcCamRef mCam;

	// resolution of the screen
	vec2 resolution;

	// fbos to render scene and composite final look
	gl::FboRef scene, compositor;

	// composer and passes to created the edging and bluring
	FxComposerRef bloomEdge;
	FxpassRef bloomPassHorizontal, bloomPassVertical, edgePass;

	// particle system
	ParticleSystem system;

	// line based particle system (not used currently)
	LineSystem lsystem;

	// Audio player to read audio and store data to textures
	AudioPlayerRef player;

	// rendering mesh plane 
	gl::VboMeshRef mMesh;
	gl::GlslProgRef mRegularShader;
	gl::BatchRef mRegBatch;

	// settings for the rendering
	VisualSettings * settings;

	// background look
	Background bg;
public:
	void setup() {
		setupSceneFbo();
		setupSceneMesh();

		settings = VisualSettings::getInstance();
		bg.setup(compositor->getColorTexture());
		//bg.setup(scene->getColorTexture());

		resolution = vec2(app::getWindowWidth(), app::getWindowHeight());

		mCam = ArcCam::create();
		
		// get instance of audio player
		player = AudioPlayer::getInstance();

		player->loadFile("audio.mp3");

		// setup cubes
		cubes.setup(player->getSpectrumSize());

		// setup particle system using starting positions based on cube positions
		system = ParticleSystem(100);
		system.setup(cubes.getPositions(),player->getSpectrumSize());
		system.setParticleSystemProperties(settings->uNumSeg, settings->uEnd, settings->uLength);

		lsystem.setup(system.getNumParticles(), 1000.0, 40.0, 400.0);
		lsystem.reset(system.getFbo());

		// =================== EVENTS ========================== //

		keydownEvent = [=](app::KeyEvent e){

			if (e.getNativeKeyCode() == 81) {

				if (player->isPlayingAudio()) {
					player->pause();
				}
				else {
					player->play();
				}
			}

		};

		// this is a replacement for drawing function
		drawFunc = [=] {
			gl::ScopedDepthTest depth(true);

			system.update();
			lsystem.save(system.getFbo());

			// update player
			player->update();

			// =================== DRAW ================ //
			gl::clear(Color(0, 0, 0));

		
			updateScene();
			updatePostProcessing();
			drawScene();
			bg.draw();
			// draw composited texture
			//gl::draw(compositor->getColorTexture(), app::getWindowBounds());

		};
	}

	// run post processing updates
	void updatePostProcessing() {
		bloomEdge->update([=](int index, FxpassRef pass)->void {

			if (index == 0) {
				pass->uniform("resolution", vec2(app::getWindowWidth(), app::getWindowHeight()));
				
			}

			// horizontal blur
			if (index == 1) {
				pass->uniform("sample_offset", vec2(1.0f / app::getWindowWidth(), 0.0f));
			}

			// vertical blur
			if(index == 2){
				pass->uniform("sample_offset", vec2(0.0f, 1.0f / app::getWindowHeight()));
			}

			pass->update();

		});
	}


	// draw composite scene
	void drawScene() {
	
		gl::disableDepthRead();
		gl::disableDepthWrite();

		gl::ScopedBlend blend(true);
		gl::ScopedBlendAdditive ba;
		gl::ScopedDepthTest depth(true);

		// bind textures
		gl::ScopedTextureBind tex0(scene->getColorTexture(), 0);


		gl::ScopedFramebuffer fbo(compositor);
		gl::clear(ColorA(0, 0, 0, 0));

		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());

		gl::draw(scene->getColorTexture(), app::getWindowBounds());
		gl::draw(bloomEdge->getOutput(), app::getWindowBounds());

		gl::enableDepth();
		gl::enableDepthRead();
		gl::enableDepthWrite();
	}

	void updateScene() {

		gl::ScopedFramebuffer fbo(scene);
		gl::ScopedDepthTest test(true);
		gl::clear(Color::black());
		gl::color(Color::white());

		mCam->useMatrices();

		cubes.draw(player->getTexture());
		system.draw(player->getTexture());

		lsystem.render(mCam, system.getFbo()->getTexture2d(GL_COLOR_ATTACHMENT2));

	}

	void setupSceneMesh() {
		gl::GlslProg::Format fmt, fmt2, fmt3;
		fmt.vertex(app::loadAsset("passthru.glsl"));
		fmt.fragment(app::loadAsset("texture.frag"));

		fmt2.vertex(app::loadAsset("passthru.glsl"));
		fmt2.fragment(app::loadAsset("bloom.glsl"));

		fmt3.vertex(app::loadAsset("passthru.glsl"));
		fmt3.fragment(app::loadAsset("edgebloom.glsl"));

		mRegularShader = gl::GlslProg::create(fmt);

		vector<vec2> positions;
		positions.push_back(vec2(-1, -1));
		positions.push_back(vec2(-1, 4));
		positions.push_back(vec2(4, -1));

		gl::VboMesh::Layout layout;
		layout.attrib(geom::POSITION, 2);

		mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout });
		mMesh->bufferAttrib(geom::POSITION, sizeof(vec2) * 3, positions.data());

		mRegBatch = gl::Batch::create(mMesh, mRegularShader);
	}

	void setupSceneFbo() {
		gl::Texture::Format tfmt;
		tfmt.setInternalFormat(GL_RGBA);

		gl::Fbo::Format fmt;
		fmt.setColorTextureFormat(tfmt);
		scene = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
		compositor = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);

	
		bloomEdge = FxComposer::create();

		bloomPassHorizontal = FxPass::create();
		bloomPassHorizontal->setup(false);
		bloomPassHorizontal->loadShader("bloom.glsl");


		bloomPassVertical = FxPass::create();
		bloomPassVertical->setup(false);
		bloomPassVertical->loadShader("bloom.glsl");

		edgePass = FxPass::create();
		edgePass->setup(false);
		edgePass->loadShader("edgebloom.glsl");


		bloomEdge->addPass(edgePass);
		bloomEdge->addPass(bloomPassHorizontal);
		bloomEdge->addPass(bloomPassVertical);

		bloomEdge->compile(scene->getColorTexture());
	}
};
