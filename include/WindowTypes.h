#pragma once

#include "cinder/gl/gl.h"
#include "cinder/Color.h"
#include <functional>
#include "cinder/app/App.h"
#include "CinderImGui.h"
#include "VisualSettings.h"
using namespace ci;
using namespace std;

class BasicWindow {

protected:
	// reference to what to run in the draw stage
	std::function<void()> drawFunc;

	std::function<void(app::KeyEvent e)> keydownEvent;
public:
	BasicWindow() {}

	virtual void setup() {
		drawFunc = [this] {

			gl::clear(Color(1, 1, 0));
		};
	}

	void setFunctions(app::WindowRef window) {
	
		if (drawFunc) {
			window->getSignalDraw().connect(drawFunc);
		}
		

		if (keydownEvent) {
			window->getSignalKeyDown().connect(keydownEvent);
		}
	}


};


class GuiWindow : public BasicWindow {
public:
	GuiWindow() {}
	
	VisualSettings * settings = VisualSettings::getInstance();

	void setup(app::WindowRef window) {
		ImGui::initialize(ImGui::Options().window(window));
		

		drawFunc = setDrawFunction();

		setFunctions(window);
	}

	std::function<void()> setDrawFunction() {
		return [this] {

			gl::clear(Color(1, 0, 0));

			
		};
	}
};