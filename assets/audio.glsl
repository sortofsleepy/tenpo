

uniform samplerBuffer uTex0;
out vec4 glFragColor;

flat in int vVertexId;
void main(){

	vec4 dat = texelFetch(uTex0,vVertexId);

	
	glFragColor = dat;
}