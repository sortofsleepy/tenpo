#version 330 core

precision highp float;
in vec3 vColor;
in float vAlpha;
out vec4 glFragColor;
void main(){
    glFragColor = vec4(vColor,vAlpha);
}
