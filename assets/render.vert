// render.vert
#version 150
precision highp float;
in vec3 ciPosition;
in int sampleIndex;

uniform mat4 ciModelViewProjection;
uniform sampler2D textureCurr;
uniform sampler2D textureNext;
uniform samplerBuffer audioTexture;

out vec4 vColor;


void main(void) {
    vec2 uv      = ciPosition.xy;
    float a = 1.0;
    vec3 currPos = texture(textureCurr,uv).rgb;
    vec3 nextPos =  texture(textureNext,uv).rgb;
	vec3 pos;
    
	vec3 audio = texelFetch(audioTexture,sampleIndex).xyz;

	float v = audio.x + audio.y + audio.z;

	if(audio.x > 0.0){
		pos = mix(currPos,nextPos,1.);
	}
	a = smoothstep(currPos.x,currPos.z,pos.x);
	gl_Position = ciModelViewProjection * vec4(pos,1.);
    gl_PointSize = 10.0;
    vColor = vec4(v) * vec4(1.0,0.0,0.0,1.0);
	
}

