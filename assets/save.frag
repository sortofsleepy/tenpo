// save.frag

#version 150
precision highp float;

in vec3 vColor;
in vec3 vExtra;


out vec4 glFragData[4];

void main(void) {
    glFragData[0] = vec4(vColor, 1.0);
    glFragData[1] = vec4(0.0, 0.0, 0.0, 1.0);
    glFragData[2] = vec4(vExtra, 1.0);
    glFragData[3] = vec4(vColor, 1.0);
}