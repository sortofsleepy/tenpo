//
//  ParticleSystem.cpp
//  PixelLines
//
//  Created by Joseph Chow on 7/25/17.
//
//

#include "ParticleSystem.h"
using namespace std;
using namespace ci;

ParticleSystem::ParticleSystem(int numParticles) :numParticles(numParticles),
pLineLife(200.0),
currentFbo(0),
targetFbo(1),
time(0.0f),
uNumSeg(100.0),
uEnd(40.0),
uLength(100.0){}

void ParticleSystem::setParticleSystemProperties(float uNumSeg, float uEnd, float uLength) {
	this->uNumSeg = uNumSeg;
	this->uEnd = uEnd;
	this->uLength = uLength;
}

void ParticleSystem::setup(std::vector<ci::vec3> points,int sampleSize){

	loadSimulationShader();
    setupBuffers();
    setupData(points);
    setupRender(sampleSize);
}

void ParticleSystem::setup() {

	vector<ci::vec3> positions;
	loadSimulationShader();
	setupBuffers();
	setupData(positions);
	setupRender(0);
}

void ParticleSystem::draw(ci::gl::BufferTextureRef audio){

	rot *= rotate(toRadians(0.2f), normalize(vec3(1, 1, 1)));
	gl::ScopedModelMatrix modelScope;
	gl::multModelMatrix(rot);


	gl::ScopedTextureBind tex0(fbos[currentFbo]->getTexture2d(GL_COLOR_ATTACHMENT0), 0);
	gl::ScopedTextureBind tex1(fbos[targetFbo]->getTexture2d(GL_COLOR_ATTACHMENT0), 1);
	gl::ScopedTextureBind tex2(audio->getTarget(),audio->getId(), 2);

	mRenderBatch->getGlslProg()->uniform("textureCurr", 0);
	mRenderBatch->getGlslProg()->uniform("textureCurr", 1);
	mRenderBatch->getGlslProg()->uniform("audioTexture", 2);
	gl::ScopedState stateScope(GL_PROGRAM_POINT_SIZE, true);

	mRenderBatch->draw();
    
}

void ParticleSystem::update() {
	time += 0.01;
	auto size = fbos[0]->getSize();
	auto bounds = fbos[0]->getBounds();

	gl::ScopedFramebuffer fbo(fbos[targetFbo]); {

		gl::clear(ColorA(0, 0, 0, 0), 1);

		gl::setMatricesWindow(size);
		gl::ScopedViewport view(size);
		gl::ScopedGlslProg shd(mSimShader);

		gl::ScopedTextureBind tex0(fbos[currentFbo]->getTexture2d(GL_COLOR_ATTACHMENT0), 0);
		gl::ScopedTextureBind tex1(fbos[currentFbo]->getTexture2d(GL_COLOR_ATTACHMENT1), 1);
		gl::ScopedTextureBind tex2(fbos[currentFbo]->getTexture2d(GL_COLOR_ATTACHMENT2), 2);
		gl::ScopedTextureBind tex3(fbos[currentFbo]->getTexture2d(GL_COLOR_ATTACHMENT3), 3);

		mSimShader->uniform("texturePos", 0);
		mSimShader->uniform("textureVel", 1);
		mSimShader->uniform("textureExtra", 2);
		mSimShader->uniform("textureOrigPos", 3);
		mSimShader->uniform("time", time);
		
		mSimShader->uniform("uNumSeg", uNumSeg);
		mSimShader->uniform("uEnd", uEnd);
		mSimShader->uniform("uLength", uLength);

		gl::drawSolidRect(bounds);


	}
	std::swap(targetFbo, currentFbo);
}

void ParticleSystem::setupBuffers(){
    
    gl::Fbo::Format fmt,fmt2;
    
    gl::Texture::Format texFmt;
    texFmt.wrap(GL_CLAMP_TO_EDGE);
    texFmt.setDataType(GL_FLOAT);
    texFmt.setInternalFormat(GL_RGBA32F);
    texFmt.setMinFilter(GL_NEAREST);
    texFmt.setMagFilter(GL_NEAREST);
    
	// need to do a weird way of counting - seems like textures get messed up if
	// you try to setup attachment 0 too, but it is setup by default.
	for (int i = 0; i < 4; ++i) {
		fmt.attachment(GL_COLOR_ATTACHMENT0 + i, gl::Texture2d::create(numParticles, numParticles, texFmt));
		fmt2.attachment(GL_COLOR_ATTACHMENT0 + i, gl::Texture2d::create(numParticles, numParticles, texFmt));
	}

	fbos[0] = gl::Fbo::create(numParticles, numParticles, fmt);
	fbos[1] = gl::Fbo::create(numParticles, numParticles, fmt2);
    for(int i = 0; i < 2; ++i){
     
		gl::ScopedFramebuffer fb(fbos[i]); {
			gl::clear();

			const static GLenum buffers[] = {
				GL_COLOR_ATTACHMENT0,
				GL_COLOR_ATTACHMENT1,
				GL_COLOR_ATTACHMENT2,
				GL_COLOR_ATTACHMENT3
			};

			gl::drawBuffers(4, buffers);
		}

    }
}

gl::TextureRef ParticleSystem::getTexture(int index) {
	return fbos[1]->getTexture2d(GL_COLOR_ATTACHMENT0 + index);
}


void ParticleSystem::drawBuffers() {
	const static GLenum buffers[] = {
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2,
		GL_COLOR_ATTACHMENT3
	};

	gl::drawBuffers(4, buffers);
}
void ParticleSystem::setupRender(int sampleSize){
    
    vector<ci::vec3> positions;
	vector<int> sampleIndex;
    for(int j = 0; j < numParticles; j++) {
        for(int i = 0; i < numParticles; i++) {
            auto ux = (float)i / numParticles;
            auto uy = (float)j / numParticles;
            positions.push_back(vec3(ux, uy, 0));

			if (sampleSize > 0) {
				sampleIndex.push_back(randInt(0, sampleSize));
			}
        }
    }
    

    gl::VboMesh::Layout layout;
    layout.attrib(geom::POSITION, 3);
	layout.attrib(geom::CUSTOM_0, 1);
    mRenderMesh = gl::VboMesh::create(positions.size(),GL_POINTS,{layout});
    mRenderMesh->bufferAttrib(geom::POSITION, sizeof(vec3) * positions.size(), positions.data());
	mRenderMesh->bufferAttrib(geom::CUSTOM_0, sizeof(int) * sampleIndex.size(), sampleIndex.data());

    gl::GlslProg::Format fmt;
    fmt.vertex(app::loadAsset("render.vert"));
    fmt.fragment(app::loadAsset("render.frag"));
    mRenderShader = gl::GlslProg::create(fmt);
    
	mRenderBatch = gl::Batch::create(mRenderMesh, mRenderShader, {
		{geom::CUSTOM_0, "sampleIndex"}
	});
    
}

void ParticleSystem::loadSimulationShader() {
	gl::GlslProg::Format fmt;
	fmt.vertex(app::loadAsset("sim.vert"));
	fmt.fragment(app::loadAsset("sim.frag"));

	mSimShader = gl::GlslProg::create(fmt);
}

void ParticleSystem::setupData(std::vector<ci::vec3> positions){
    int range = 3;

	vector<ci::vec3> pos;
    vector<ci::vec2> coords;
    vector<ci::vec3> extras;

	int size = numParticles * numParticles;
	if (positions.size() < 1) {
		for (int a = 0; a < size; ++a) {
			pos.push_back(vec3(mRand.randFloat(-range, range), mRand.randFloat(-range, range), mRand.randFloat(-range, range)));
		}
	}
	else {
	
		for (int a = 0; a < size; ++a) {
			pos.push_back(positions[mRand.randInt(0, positions.size())]);
		}
	}
    
    for(int j = 0; j < numParticles; j++) {
        for(int i = 0; i < numParticles; i++) {
            float ux = (float)i / numParticles * 2.0 - 1.0 + .5 / numParticles;
            float uy = (float)j / numParticles * 2.0 - 1.0 + .5 / numParticles;
            
            extras.push_back(vec3(mRand.randFloat(), mRand.randFloat(), mRand.randFloat(1, pLineLife)));
            coords.push_back(vec2(ux, uy));

        }
    }

	originalPositions = gl::Texture::create(pos.data(), GL_RGBA, numParticles, numParticles);
	
    gl::GlslProg::Format dataShd;
    dataShd.vertex(app::loadAsset("save.vert"));
    dataShd.fragment(app::loadAsset("save.frag"));
	dataShd.attrib(geom::CUSTOM_0, "aExtra");
    mDataShader = gl::GlslProg::create(dataShd);
    
    gl::VboMesh::Layout layout;
    layout.attrib(geom::POSITION, 3);
    layout.attrib(geom::TEX_COORD_0, 2);
    layout.attrib(geom::CUSTOM_0, 3);
    mDataMesh = gl::VboMesh::create(pos.size(),GL_POINTS,{layout});
    mDataMesh->bufferAttrib(geom::POSITION, sizeof(vec3) * pos.size(), pos.data());
    mDataMesh->bufferAttrib(geom::TEX_COORD_0, sizeof(vec2) * coords.size(), coords.data());
    mDataMesh->bufferAttrib(geom::CUSTOM_0, sizeof(vec3) * extras.size(), extras.data());
    
	mDataBatch = gl::Batch::create(mDataMesh, mDataShader);
	
    const static GLenum buffers[] = {
        GL_COLOR_ATTACHMENT0,
        GL_COLOR_ATTACHMENT1,
        GL_COLOR_ATTACHMENT2,
        GL_COLOR_ATTACHMENT3
    };
    for(int i = 0; i < 2; ++i){
        gl::ScopedFramebuffer fbo(fbos[i]);
        gl::clear(ColorA(0,0,0,0));
		gl::setMatricesWindow(fbos[0]->getSize());
		gl::viewport(fbos[0]->getSize());

        gl::drawBuffers(4, buffers);
		gl::ScopedState stateScope(GL_PROGRAM_POINT_SIZE, true);
        mDataBatch->draw();
    }
}
