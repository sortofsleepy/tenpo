#include "CubedCube.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

static ci::ColorA color_pallete[4] = {
	ColorA(76.0f,137.0f,71.0f,1.0f),
	ColorA(165.0f,222.0f,255.0f,1.0f),
	ColorA(135.0f,207.0f,248.0f,1.0f),
	ColorA(69.0f,171.0f,171.0f,1.0f),

};
CubedCube::CubedCube():time(0.0f) {}

vector<ci::vec3> CubedCube::getPositions() {
	return cubePositions;
}

void CubedCube::setup(int sampleSize,int size,int resolution) {

	// general position of all cubes.s
	vector<vec3> cubeColors;
	vector<float> rotations;
	vector<float> scaleOffset;
	vector<int> spectrumIndex;

	float cols = size / resolution;
	float rows = size / resolution;
	float sx = resolution;
	float sy = resolution;

	for (int iy = 0; iy <= cols; iy++) {
		for (int ix = 0; ix <= rows; ix++) {
			

			for (int iz = 0; iz <= cols; ++iz) {

				float x = (ix * resolution);
				float y = (iy * resolution);
				float z = (iz * resolution);

				auto half = size / 2;


				// calculate cube positions
				vec3 cubePos = vec3(x - half, y - half, z - half);
				cubePositions.push_back(cubePos);

				// calculate rotation offsets
				rotations.push_back(Rand::randFloat(-1,1));


				scaleOffset.push_back(Rand::randFloat());

				// calculate cube colors
				auto color = color_pallete[Rand::randInt(0, 3)];
				vec3 col = vec3(color.r, color.g, color.b);
				col = glm::normalize(col);
				cubeColors.push_back(col);

				// set random spectrum index
				spectrumIndex.push_back(randInt(0, sampleSize));
			}
		
		}
	}


	numInstances = cubePositions.size();

	// store cube positions
	mCubePositions = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(vec3) * cubePositions.size(), cubePositions.data(), GL_STATIC_DRAW);
	mCubeRotation = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * rotations.size(), rotations.data(), GL_STATIC_DRAW);
	mCubeColors = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(vec3) * cubeColors.size(), cubeColors.data(), GL_STATIC_DRAW);
	gl::VboRef mSpectrumIndices = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(int) * spectrumIndex.size(), spectrumIndex.data(), GL_STATIC_DRAW);
	// build cube itself
	mMesh = gl::VboMesh::create(geom::Cube().size(vec3(resolution / 2)));

	// append instanced data
	geom::BufferLayout instanceDataLayout,instanceDataLayout2,instanceDataLayout3,instanceDataLayout4;
	instanceDataLayout.append(geom::CUSTOM_0, 3, 0, 0, 1 /* per instance */);
	instanceDataLayout2.append(geom::CUSTOM_1, 1, 0, 0, 1);
	instanceDataLayout3.append(geom::CUSTOM_2, 3, 0, 0, 1);
	instanceDataLayout4.append(geom::CUSTOM_3, 1, 0, 0, 1);

	mMesh->appendVbo(instanceDataLayout, mCubePositions);
	mMesh->appendVbo(instanceDataLayout2, mCubeRotation);
	mMesh->appendVbo(instanceDataLayout3, mCubeColors);
	mMesh->appendVbo(instanceDataLayout4, mSpectrumIndices);
	

	gl::GlslProg::Format fmt;
	fmt.vertex(app::loadAsset("cube.vert"));
	fmt.fragment(app::loadAsset("cube.frag"));
	mShader = gl::GlslProg::create(fmt);

	mBatch = gl::Batch::create(mMesh, mShader, {
		{geom::CUSTOM_0, "instancePosition"},
		{geom::CUSTOM_1, "instanceRotation"},
		{geom::CUSTOM_2, "instanceColor"},
		{geom::CUSTOM_3, "instanceSpectrumIndex"}
	});
}

void CubedCube::draw(ci::gl::BufferTextureRef audio) {
	time += 0.01f;
	gl::ScopedDepth depth(true);
	gl::ScopedGlslProg shd(mShader);

	// rotate cube
	rot *= rotate(toRadians(0.2f), normalize(vec3(1, 1, 1)));
	gl::ScopedModelMatrix modelScope;
	gl::multModelMatrix(rot);

	gl::ScopedTextureBind scopeTex(audio->getTarget(), audio->getId(),0);

	// apply uniforms
	//gl::draw(audio, Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));

	mShader->uniform("spectrumSize", sampleSize);
	mShader->uniform("randomVal", Rand::randVec2());
	mShader->uniform("time", time);
	mShader->uniform("scaleVal", 0);
	// draw
	mBatch->drawInstanced(numInstances);
	//mBatch->draw();
	/*
	mShader->uniform("time", time);
	mShader->uniform("scaleVal", 0);
	// draw
	mBatch->drawInstanced(numInstances);
	//mBatch->draw();
	
	*/


}